# Instructions To Get Running
* Firstly, have running the API Server ([Netshoes Cart API Server](https://bitbucket.org/somente-eu/teste-netshoes-server))
* Download or Clone Git Repository
* Run `npm install`
* Run `npm start`

# Running Tests

You may run the tests by running `npm test`

# Source Code

Source code is located under the `src` folder.

# Elements Used

This web app is builted using ReactJS, React Scrollbar, Axios, LocalStorage and Noty.
import React from 'react';

class StorageService extends React.Component {
    componentWillMount() {
    }

    set(key, value) {
        localStorage.setItem(key, JSON.stringify(value));
    }

    get(key) {
        return JSON.parse(localStorage.getItem(key));
    }

    del(key) {
        return localStorage.removeItem(key);
    }
}

export default StorageService
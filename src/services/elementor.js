import StorageService from '../services/storage'

export default class Elementor {
    static Header
    static Cart
    static Storage = new StorageService()

    static toggleCart = () => {
        const body = document.body
        let cartOpenned = body.classList.contains('cart-open')
        if (cartOpenned) {
            body.classList.remove('cart-open')
        } else {
            body.classList.add('cart-open')
        }
    }

}
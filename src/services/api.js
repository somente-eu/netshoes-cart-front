import { API_URL } from '../settings';

import React from 'react';
import axios from 'axios';

export default class ApiService extends React.Component {
    async get(url, data = {}) {
        try {
            const response = await axios.get(`${API_URL}/${url}`, {
                params: data,
            })
            return response;
        } catch (error) {
            console.error(error);
        }
    }
}
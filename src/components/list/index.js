import React from 'react'
import ApiService from '../../services/api'
import './list.css'
import Elementor from '../../services/elementor';

export default class ProductList extends React.Component {

    constructor() {
        super()
        this.Api = new ApiService()

        this.state = {
            data: false,
        }
    }

    componentDidMount() {
        this.loadProducts()
    }

    async loadProducts() {
        let req = await this.Api.get('products/list')
        if (req && req.data) {
            let data = req.data
            this.setState({ data })
        }
    }

    addToCart(product, size, qtd) {
        let items = Elementor.Cart.addToCart(product, size, qtd)
        Elementor.Cart.setState({ items })
    }

    getProductGrid() {
        const { data } = this.state
        if (data.products) {
            let products = Object.values(data.products).map((product, i) => {

                const price = product.price.toFixed(2).toString().split('.')
                const installmentPrice = (product.price / (product.installments > 1 ? product.installments : 1)).toFixed(2).toString().replace('.', ',')
                const { availableSizes } = product

                let sizes = availableSizes.map((size, i) => {
                    return (
                        <button key={i} className="btn btn-default" onClick={() => { this.addToCart(product, size, 1) }}>
                            {size}
                        </button>
                    )
                })

                return (
                    <div key={product.id} className="col-xs-12 col-sm-4 product">
                        <div className="product-thumb">
                            <img alt="Product" src="/img/product-image-placeholder.jpg" />

                            <div className="product-overlay">
                                <div className="product-actions">
                                    <button className="btn btn-default">
                                        <i className="fas fa-plus-circle"></i>
                                        Adicionar ao carrinho
                                    </button>

                                    <div className="sizes">
                                        {sizes}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="product-title">
                            {product.title}
                        </div>
                        <hr />
                        <div className="product-price">
                            <span className="cointype">R$</span> {price[0]}<span className="minor">,{price[1]}</span>
                            {
                                product.installments > 1 ? (
                                    <div className="product-installments">
                                        ou {product.installments}x <span>R$ {installmentPrice}</span>
                                    </div>
                                ) : (null)
                            }
                        </div>
                    </div>
                )
            })

            return products
        }
    }

    render() {
        return (
            <div className="list">
                <div className="container">
                    <div className="row">
                        {this.getProductGrid()}
                    </div>
                </div>
            </div>
        )
    }
}
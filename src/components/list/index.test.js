import React from 'React'
import List from './index'
import Elementor from '../../services/elementor';
import Cart from '../cart';

it('should load data from json and return products list', async () => {
    const list = new List()
    list.loadProducts()
})

it('should render product list without crash, even if no data is loaded', () => {
    let list = new List()
    list.state = {
        data: {
            "products": [
                {
                    "id": 0,
                    "sku": 8552515751438644,
                    "title": "Camisa Nike Corinthians I",
                    "description": "14/15 s/nº",
                    "availableSizes": [
                        "S",
                        "G",
                        "GG",
                        "GGG"
                    ],
                    "style": "Branco com listras pretas",
                    "price": 229.9,
                    "installments": 9,
                    "currencyId": "BRL",
                    "currencyFormat": "R$",
                    "isFreeShipping": true
                },
                {
                    "id": 1,
                    "sku": 18644119330491312,
                    "title": "Camisa Nike Corinthians II",
                    "description": "14/15 s/nº",
                    "availableSizes": [
                        "S",
                        "G",
                        "GG",
                        "GGG"
                    ],
                    "style": "Preta com listras brancas",
                    "price": 229.9,
                    "installments": 9,
                    "currencyId": "BRL",
                    "currencyFormat": "R$",
                    "isFreeShipping": true
                }
            ]
        }
    }
    list.getProductGrid()

    list.state = { data: false }
    list.getProductGrid()
})



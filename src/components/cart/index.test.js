import React from 'React'
import Cart from './index'
import ReactDOM from 'react-dom';
import Elementor from '../../services/elementor';
import Header from '../header';

it('should add product to cart and return items', async () => {
    Elementor.Header = new Header()
    const cart = new Cart()
    let items = cart.addToCart({
        id: 1,
        title: 'Teste',
        description: 'Teste.',
        style: 'Style 1',
        price: 250.9,
        qtd: 1
    }, 'G', 1)

    console.log(items)
})

it('should remove product to cart and return items', async () => {
    Elementor.Header = new Header()
    const cart = new Cart()
    let items = cart.addToCart({
        id: 1,
        title: 'Teste',
        description: 'Teste.',
        style: 'Style 1',
        price: 250.9,
        qtd: 1
    }, 'G', 1)

    items = cart.removeFromCart(1, 'G')

    console.log(items)
})

it('should get and render item list and subtotal with no problems', async () => {
    Elementor.Header = new Header()
    const cart = <Cart />
    Elementor.Cart.addToCart({
        id: 1,
        title: 'Teste',
        description: 'Teste.',
        style: 'Style 1',
        price: 250.9,
        qtd: 1
    }, 'G', 1)
    const div = document.createElement('div');
    ReactDOM.render(cart, div);
})
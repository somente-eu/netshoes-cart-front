import React from 'react'
import Elementor from '../../services/elementor';
import './cart.css'
import ScrollArea from 'react-scrollbar'

import noty from 'noty'

export default class Cart extends React.Component {
    constructor(props) {
        super(props)

        Elementor.Cart = this
        let items = Elementor.Storage.get('items')

        this.state = {
            items: items ? items : [],
            hover: -1,
        }
    }

    addToCart(product, size, qtd) {
        let p = {
            id: product.id,
            title: product.title,
            description: product.description,
            size: size,
            style: product.style,
            price: product.price,
            qtd: qtd
        }

        let qtdUpdated = false
        const { items } = this.state

        for (let i = 0; i < items.length; i++) {
            let item = items[i]
            if (item.id === p.id && item.size === p.size) {
                item.qtd += qtd
                qtdUpdated = true
            }
        }

        if (!qtdUpdated) {
            items.push(p)
        }

        Elementor.Storage.set('items', items)
        Elementor.Header.forceUpdate()

        new noty({
            theme: 'metroui',
            text: '<i class="fas fa-plus-circle"></i> &nbsp; &nbsp; Produto adicionado ao carrinho!',
            timeout: 3000,
            type: 'success',
            layout: 'topCenter'
        }).show();

        return items
    }

    removeFromCart(id, size) {
        const { items } = this.state

        for (let i = 0; i < items.length; i++) {
            let item = items[i]
            if (item.id === id && item.size === size) {
                items.splice(i, 1)
            }
        }

        Elementor.Storage.set('items', items)
        Elementor.Header.forceUpdate()

        new noty({
            theme: 'metroui',
            text: '<i class="fas fa-times"></i> &nbsp; &nbsp; Produto removido do carrinho!',
            timeout: 3000,
            type: 'success',
            layout: 'topCenter'
        }).show();

        return items
    }

    getItemList() {
        const { items, hover } = this.state

        if (items.length === 0) {
            return (<div className="item text-center">Nenhum item adicionado</div>)
        }

        let list = items.map((item, i) => {
            const price = item.price.toFixed(2).toString().split('.')
            return (
                <div key={i} className="item">
                    <div className={`content${hover === item.id ? ` remove` : ''}`}>
                        <div className="item-img" style={{ backgroundImage: 'url(/img/product-image-placeholder.jpg)' }}></div>
                        <div className="item-data">
                            <div className="title">{item.title}</div>
                            <div className="details">{item.size} | {item.style}</div>
                            <div className="details">Quantidade: {item.qtd}</div>
                        </div>
                        <div className="item-value">
                            <div className="remove" onMouseEnter={() => { this.setState({ hover: item.id }) }} onMouseLeave={() => { this.setState({ hover: -1 }) }} onClick={() => { let items = this.removeFromCart(item.id, item.size); this.setState({ items, hover: -1 }) }}>
                                <i className="fas fa-times"></i>
                            </div>
                            <div className="price">
                                R$ <span>{price[0]}</span>,{price[1]}
                            </div>
                        </div>

                    </div>
                </div>
            )
        })

        return list
    }

    getSubtotal() {
        const { items } = this.state

        let value = 0

        items.map((item, i) => {
            value += (item.price * item.qtd)
            return (0)
        })

        const price = value.toFixed(2).toString().split('.')
        const installment = (value / 10).toFixed(2).replace('.', ',')

        return (
            <div className="subtotal">
                <h5>Subtotal</h5>

                <div className="value">
                    <div className="price">R$ <span>{price[0]}</span>,{price[1]}</div>
                    <div className="installments">Ou em até 10x de {installment}</div>
                </div>
            </div>
        )
    }

    handleMouseLeave(e) {
        let pos = {
            x: e.clientX,
            y: e.clientY
        }

        let offset = document.getElementById('cart').getBoundingClientRect();
        let offsetX = offset.x + offset.width
        console.log(offset)
        console.log(pos, offsetX)

        let fromRight = (pos.x >= offsetX ? true : false);

        if (!fromRight) {
            Elementor.toggleCart()
        }
    }

    render() {
        const { items } = this.state
        return (
            <div id="cart" className="aside-cart" onMouseLeave={(e) => { this.handleMouseLeave(e) }}>
                <ScrollArea
                    speed={0.8}
                    className="scrollbar-view"
                    contentClassName="scroll-content"
                    horizontal={false}
                    smoothScrolling={true}
                >
                    <div className="head">
                        <div className="bag">
                            <div className="count">{items.length}</div>
                        </div>
                        <h2>Sacola</h2>
                    </div>

                    <div className="list">
                        {this.getItemList()}
                    </div>

                    {this.getSubtotal()}

                    <button className="btn btn-black btn-block">Comprar</button>
                </ScrollArea>
            </div>
        )
    }
}
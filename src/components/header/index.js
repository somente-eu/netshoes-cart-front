import React from 'react'
import './header.css'
import Elementor from '../../services/elementor'

export default class Header extends React.Component {
    constructor() {
        super()

        Elementor.Header = this
    }

    render() {
        let items = Elementor.Storage.get('items') ? Elementor.Storage.get('items') : {}

        console.log(items)

        return (
            <div className="header">
                <div className="container">
                    <div className="left">
                        <img src="img/logo.png" alt="Logo" />
                    </div>
                    <div className="right">
                        <div className="bag" onClick={() => { Elementor.toggleCart() }}>
                            <div className="count">{items.length}</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
import React, { Component } from 'react';
import Products from './components/list'
import Cart from './components/cart'
import Header from './components/header';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Cart />
        <Products />
      </div>
    );
  }
}

export default App;
